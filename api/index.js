'use strict';
const express = require('express');
const app = express();
const nodemailer = require("nodemailer");
const fs = require("fs");

//const multer = require('multer');
//const upload = multer();

const port = 5000;

const https = require('https')

var privateKey = fs.readFileSync( '/etc/letsencrypt/live/api.razumvdom.ru/privkey.pem' );
var certificate = fs.readFileSync( '/etc/letsencrypt/live/api.razumvdom.ru/fullchain.pem' );

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//app.use(upload.array()); 
app.use(express.static('public'));

async function mail(req) {
  console.log(req.body)
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
//  let testAccount = await nodemailer.createTestAccount();

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.yandex.ru",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: "robot@wsweb.ru", // generated ethereal user
      pass: "Iamrobot404", // generated ethereal password
    },
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"Robot" <robot@wsweb.ru>', // sender address
    to: "d.nosovitsky@gmail.com, slavitto@gmail.com", // list of receivers
    subject: "Заполнение формы razumvdom.ru", // Subject line
    //text: JSON.stringify(req.body), // plain text body
    html: req.body.firstName + " " + req.body.lastName + "<br>" + req.body.contactInfo_Phone_1 + "<br>" + req.body.contactInfo_Email_1 + "<br>" + req.body.about, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.get('/ping/', (req, res, next) => {

  res.status(200);
  res.send('pong');

});

app.post('/order/', function (req, res) {
    mail(req).catch(console.error);
 //   res.send(req.body);
  res.writeHead(302, {
    'Location': 'https://razumvdom.ru/thanks'
  });
  res.end();
});

app.use((req, res, next) => {
  res.status(404).send('Path not found');
});

app.use((err, req, res, next) => {
  res.status(500);
  res.render('error', { error: err });
  next(err);
});

app.listen(port, () => { console.log('Api listening on ' + port ) });

https.createServer({
    key: privateKey,
    cert: certificate
}, app).listen(5443, () => console.log('HTTPS on 5443'));

