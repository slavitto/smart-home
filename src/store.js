import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    showPopup: false,
    price: '69 990',
  },
  mutations: {
    setDataProp(state, prop) {
      state[Object.keys(prop)] = Object.values(prop)[0];
    },
  }
});