import Vue from 'vue'
import VueRouter from 'vue-router'
import App, { router } from './App.vue'
import './assets/js/jquery-3.4.0.min.js'
// import './assets/js/snippet.js' // всплывающие окна
import './assets/js/plugins.min.js'
import './assets/js/scripts.js'
import './assets/js/jquery.maskedinput.min.js'
import store from './store'

Vue.use(VueRouter)

Vue.config.productionTip = false


new Vue({
	router,
	store,
  	render: h => h(App),
}).$mount('#app')
