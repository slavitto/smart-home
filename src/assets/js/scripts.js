var Main = {
    init: function () {
        this.Tabs();
        // this.Popups.init();
        // this.Calculate();
        // this.WillBeAnimate.init();
        // this.Faq();
        this.SendForm();
    },
    Tabs: function () {
        $('.tabs__item_active').fadeIn(0);

        $('.tabs__btn').on('click', function(e){
            e.preventDefault();

            var $self = $(this);
            var id = $self.attr('href');
            var $btns = $self.closest('.tabs__panel').find('.tabs__btn');
            var $tab = $(id);
            var $activeTab = $tab.closest('.tabs__content').find('.tabs__item_active');

            $btns.removeClass('tabs__btn_active');
            $self.addClass('tabs__btn_active');

            $activeTab.removeClass('tabs__item_active').fadeOut(200, function(){
                $tab.addClass('tabs__item_active').fadeIn(200);
            });
        });
    },
    SendForm: function () {
        $.validator.addMethod(
            'phone',
            function (value, element, params) {
                return /(\+{0,1}7|8)\s\((?!89)\d{3}\)\s\d{3}-\d{2}-\d{2}/.test(value);
            },
            'Проверьте корректность номера телефона'
        );
        var validateSetting = {
            ignore: [],
            rules: {
                name: "required",
                phone: {
                    required: true,
                    phone: true
                },
                email: "required"
            },
            messages: {
                name: "Пожалуйста, заполните это поле",
                phone: {
                    required: "Пожалуйста, заполните это поле"
                },
                email: "Пожалуйста, заполните это поле"
            },
            // submitHandler: function (form) {
            //     $('button[type=submit]').attr('disabled', true);
            //     var formData = new FormData(form);
            //     formData.append('room', $('.form-radio__inp:checked').eq(0).val());
            //     formData.append('square', $('.form-radio__inp:checked').eq(1).val());

            //     formData.append('lighting', $('input[name=lighting]').prop('checked') ? 1 : 0);
            //     formData.append('security', $('input[name=security]').prop('checked') ? 1 : 0);
            //     formData.append('comfort', $('input[name=comfort]').prop('checked') ? 1 : 0);

            //     formData.append('act', 'create-request');
            //     digitalData.events.push({
            //         name: 'Sent Smart Home Application Form',
            //         category: 'Behaviour',
            //         label: 129980, // предварительная оценка стоимости
            //         user: {
            //             firstName: formData.get('name'),
            //             email: formData.get('email'),
            //             phone: formData.get('phone').replace(/\D/g, '')
            //         }
            //     });
            //     $.ajax({
            //         data: formData,
            //         processData: false,
            //         contentType: false,
            //         dataType: 'json',
            //         type: 'post',
            //         url: $(form).attr('action'),
            //         success: function (data) {
            //             /*if (data.added) {
            //                 $('#popup-title').show();
            //                 $('#popup-ok').show();
            //                 $('#popup-error').hide();
            //             } else {
            //                 $('#popup-title').hide();
            //                 $('#popup-ok').hide();
            //                 $('#popup-error').html(data.error);
            //                 $('#popup-error').show();
            //             }
            //             Main.Popups.open($('#done'));
            //             $('button[type=submit]').removeAttr('disabled');*/
            //         },
            //         error: function (jqXhr, textStatus, errorThrown) {
            //             console.log(errorThrown);
            //         }
            //     });

            //     $('#popup-title').show();
            //     $('#popup-ok').show();
            //     $('#popup-error').hide();
            //     Main.Popups.open($('#done'));
            //     $('button[type=submit]').removeAttr('disabled');
            // }
        };

        $("#request-form").submit(function (e) {
            e.preventDefault();
        }).validate(validateSetting);

        $("#request-form-popup").submit(function (e) {
            e.preventDefault();
        }).validate(validateSetting);
    }
};

$(function () {
    Main.init();
});


$('.inputfile').on('change', function() {

    var files = $(this)[0].files;
    var values = $('.for-value');
    var $valueContainer = $(this).closest('.files-add').find('.file-add_container');

    if ($valueContainer.children(values).length > 0) {
        values.remove();
    }

    for (var i = 0; i < files.length; i++) {
        takeName(files[i].name, $valueContainer);
    }
    $(".close-cross").on('click', function(e) {
        var $self = $(e.target);
        if($self.hasClass("close-cross")) {
            $self.closest('.for-value').remove();
        }
    });
});



function takeName(name, container) {
    var markup = $("<div class='for-value'>" +
        "<div class='sheet'><div class='sheet-triangle'></div></div>" +
        "<div class='val-container'>"+ name +"</div>" +
        "<div class='close-cross'></div>" +
        "</div>");

    container.append(markup);
}


function redirect() {
    $('#svg-home').on('click', function () {
        console.log('link-1');
        window.location = '/smart-home/';
    });

    $('#svg-main').on('click', function () {
        console.log('link');
        window.location = '/';
    });
}
redirect();

function showHideHint() {
    $('#svg-main').on('mouseenter', function () {
        $('.header__logo-hint').show();
    });
    $('#svg-main').on('mouseleave', function () {
        $('.header__logo-hint').hide();
    });
}
showHideHint();

// $('body').one('focus.textarea', '.form-area_comment', function(e) { //<<<--- comment validation (?)
//         baseH = this.scrollHeight;}).on('input.textarea', '.form-area_comment', function(e) {
//         if(baseH < this.scrollHeight) {
//             $(this).height(0).height(this.scrollHeight);
//         }
//         else {
//             $(this).height(0).height(baseH);
//         }
//     });



$('.inputfile').on("change", function() {

    var markupError = $("<div class='error-value'>" + "Вы приложили более 5 файлов!" + "</div>");


    if ($(".inputfile")[0].files.length > 5) {
        $('.file-add_container').append(markupError);
        $('.for-value').remove();
        $('.inputfile').val('');
    } else {
        $('.error-value').remove();
    }
});
